package ad2.ss17.cflp;

import java.util.Arrays;

/**
 * Klasse zum Berechnen der L&ouml;sung mittels Branch-and-Bound.
 * Hier sollen Sie Ihre L&ouml;sung implementieren.
 */
public class CFLP extends AbstractCFLP {

    private int distanceCost;
    private int[] openingCosts;
    private int[] maxBandwidths;
    private int[] bandwidthsCustomer;
    private int[][] distances;
    private int numOfCustomer;
    private int facilityNum;
    private int upperBound;
    private int lowerBound;
    private int[] currentSolution;
    private int [][] orderedFacilities;
    private int[][] orderedDistances;

    public CFLP(CFLPInstance instance) {
        // TODO: Hier ist der richtige Platz fuer Initialisierungen
        distanceCost = instance.distanceCosts;
        openingCosts = instance.openingCosts;
        maxBandwidths = instance.maxBandwidths;
        bandwidthsCustomer = instance.bandwidths;
        distances = instance.distances;
        numOfCustomer = instance.getNumCustomers();
        facilityNum = instance.getNumFacilities();
        currentSolution = new int[numOfCustomer];

        for (int i = 1; i < numOfCustomer ; i++) {
            currentSolution[i] = -1;
        }
        orderedFacilities = new int[facilityNum][numOfCustomer];
        orderedDistances = new int[facilityNum][numOfCustomer];
        for (int i = 0; i < numOfCustomer; i++) {
            for (int j = 0; j < facilityNum; j++) {
                orderedFacilities[j][i] = j;
                orderedDistances[j][i] = distances[j][i];
            }
        }
        orderFacilities();
        int[] firstSoultion = new int[numOfCustomer];
        for (int i = 0; i < firstSoultion.length; i++) {
            firstSoultion[i] = orderedFacilities[0][i];
        }
        upperBound = calcObjectiveValue(firstSoultion);
        lowerBound = 0;

    }



    // set facilities for each customer as their distance to facility increase
    private void orderFacilities(){
        for (int i = 0; i < numOfCustomer; i++) {
            //selection sort
            for (int j = 0; j <= distances.length-2; j++) {
                int smallest = j;
                for (int l = j + 1; l <= distances.length-1; l++) {
                    if (orderedDistances[l][i] < orderedDistances[smallest][i]) {
                        smallest = l;
                    }
                }
                //swap positions
                int help = orderedDistances[j][i];
                orderedDistances[j][i] = orderedDistances[smallest][i];
                orderedDistances[smallest][i] = help;
                help = orderedFacilities[j][i];
                orderedFacilities[j][i] = orderedFacilities[smallest][i];
                orderedFacilities[smallest][i] = help;
            }
        }
    }

    // factor() iterative
    private  int factor(int k, int baseCosts){
        switch (k) {
            case 0:
                return 0;
            case 1:
                return baseCosts;
            case 2:
                return (int) Math.ceil(1.5 * baseCosts);
            default:
                int c1 = baseCosts;
                int c2 = (int) Math.ceil(1.5 * baseCosts);
                for (int i = 3; i <= k; i++) {
                    int cost = Math.addExact((Math.addExact(c1,c2)),((4-i) * baseCosts));
                    c1 = c2;
                    c2 = cost;
                }
                return c2;
        }
    }

    /**
     * Diese Methode bekommt vom Framework maximal 30 Sekunden Zeit zur
     * Verf&uuml;gung gestellt um eine g&uuml;ltige L&ouml;sung
     * zu finden.
     * <p>
     * <p>
     * F&uuml;gen Sie hier Ihre Implementierung des Branch-and-Bound-Algorithmus
     * ein.
     * </p>
     */
    @Override
    public void run() {
        // TODO: Diese Methode ist von Ihnen zu implementieren
        branchAndBound(0);
    }

    private void branchAndBound(int customer){
        if (customer<numOfCustomer){
            for (int i = 0; i < facilityNum; i++) {
                currentSolution[customer] = orderedFacilities[i][customer];

                int unassignedFacilities = 0;
                if (customer != numOfCustomer-1) {
                    for (int j = 0; j < numOfCustomer; j++) {
                        if (currentSolution[j] == -1) {
                            unassignedFacilities += orderedDistances[0][j] * distanceCost;
                        }
                    }
                }
                lowerBound = unassignedFacilities + calcObjectiveValue(currentSolution);
                if (lowerBound<=upperBound){
                    branchAndBound(customer+1);
                }
                currentSolution[customer] = -1;
            }
        }
        else {
            int currentBound = calcObjectiveValue(currentSolution);
            if (currentBound<=upperBound){
                setSolution(currentBound,currentSolution);
                upperBound = currentBound;
            }
        }
    }

    private int calcObjectiveValue(int[] solution) {
        boolean[] openedFacilities = new boolean[facilityNum];
        Arrays.fill(openedFacilities, false);

        if (solution.length != numOfCustomer)
            throw new RuntimeException("Problem beim Ermitteln des Zielfunktionswertes (zu wenige/zu viele Kunden)");

        int[] accBandwidths = new int[facilityNum];
        for (int i = 0; i < solution.length; ++i) {
            if (solution[i] < 0) continue;
            accBandwidths[solution[i]] += bandwidthsCustomer[i];
        }

        int sumCosts = 0;
        for (int i = 0; i < solution.length; ++i) {
            if (solution[i] < 0) continue;

            if (!openedFacilities[solution[i]]) {
                sumCosts = Math.addExact(sumCosts, factor((int) Math.ceil(accBandwidths[solution[i]] / (double) maxBandwidths[solution[i]]), openingCosts[solution[i]]));
                openedFacilities[solution[i]] = true;
            }
            sumCosts = Math.addExact(sumCosts, distanceCost * distances[solution[i]][i]);
        }
        return sumCosts;
    }

}